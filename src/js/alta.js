  class MyAlta extends Polymer.Element {
    static get is() { return 'my-alta'; }

  static get properties() {
    return {
      data:{
          type: Object,
          value:{},
          computed: 'bienvenido()'
      },
      amigo:{
        type: String
      }
    };
  }
  constructor() {
    super();
  }
  bienvenido(){
    var ddata=document.querySelector("my-app").data;
  return ddata;
  }
  cancelar (){
    document.querySelector("my-app").page ='my-view3';
  }
  creaAlta (){
    var objalta={};
    objalta.numclient=this.$.numclien.value;
    objalta.nombre=this.$.nombre.value;
    objalta.fecha_ingreso=this.$.fecha1.value;
    objalta.password=this.$.pass.value;
    objalta.admin=false;
    objalta.RFC=this.$.rfc.value;
    objalta.direcciones=[1];
    objalta.direcciones[0]={}
    objalta.direcciones[0].calle=this.$.calle.value;
    objalta.direcciones[0].colonia=this.$.colonia.value;
    objalta.direcciones[0].delegacion=this.$.delegacion.value;
    objalta.direcciones[0].estado=this.$.estado.value;
    objalta.direcciones[0].tipo="P";
    objalta.telefonos=[1];
    objalta.telefonos[0]={}
    objalta.telefonos[0].tipo="C";
    objalta.telefonos[0].lada=this.$.ladaTel.value;
    objalta.telefonos[0].telefono=this.$.telefono.value;
    objalta.contratos=[1];
    objalta.contratos[0]={}
    objalta.contratos[0].contrato=this.$.numerocontrato.value;
    objalta.contratos[0].plastico=this.$.numerotarjeta.value;
    objalta.contratos[0].tipo=this.$.tipotarjeta.value;
    objalta.contratos[0].saldo=this.$.saldoInicial.value;
    objalta.Movimientos=[1];
    objalta.Movimientos[0]={}
    objalta.Movimientos[0].contrato=this.$.numerocontrato.value;
    objalta.Movimientos[0].fecha=this.$.fecha1.value;
    objalta.Movimientos[0].importe=this.$.saldoInicial.value;
    objalta.Movimientos[0].tipo="A";
    objalta.Movimientos[0].desTipo="Abono";
    objalta.Movimientos[0].descripcion="Saldo Inicial";
    objalta.Movimientos[0].descripcion2="NA";

    objalta=JSON.stringify(objalta);
    console.log(objalta);


      var cadena = " http://localhost:1987/alta/";
      console.log(cadena);
      var request = new XMLHttpRequest();
      request.withCredentials = true;
      request.addEventListener("readystatechange", function () {
        if (this.readyState === 4) {
          console.log(this.responseText);
        }
      });
      request.open("post", cadena , true);
      request.setRequestHeader("Accept","application/json");
      request.setRequestHeader("content-type", "application/json");
      request.setRequestHeader("cache-control", "no-cache");

      request.send(objalta);

      if (request.status==200){
        console.log('si paso!');

      }else{
        console.log(request.status);
      }
      document.querySelector("my-app").page ='my-view3';


  }

}
  window.customElements.define(MyAlta.is, MyAlta);
