  class MySaldos extends Polymer.Element {
    static get is() { return 'my-saldos'; }
    static get properties() {
      return {
        data:{
            type: Object,
            value:{},
            computed: 'bajainfo()'
        }
      };
    }

    bajainfo(){
      var i=0;
      var ddata=document.querySelector("my-app").data;
      console.log("aquillego");
      while(i<ddata[0].contratos.length){
          ddata[0].contratos[i].conversionDolar= this.convierte(ddata[0].contratos[i].saldo,"MXN","USD");
          ddata[0].contratos[i].conversionEUR= this.convierte(ddata[0].contratos[i].saldo,"MXN","EUR");
          ddata[0].contratos[i].saldoformateado= this.formato(ddata[0].contratos[i].saldo);
          ddata[0].contratos[i].enmascarada="************"+ddata[0].contratos[i].contrato.substring(11,15);
          i++;
        }
        return ddata;
   }


convierte (monto, from, to){
  var valor;
  var resultado;
  var request = new XMLHttpRequest();
  var cadena = " http://localhost:1987/mySaldos/"+from+","+to+","+monto;
  request.open("GET", cadena ,false);
  request.setRequestHeader("Accept","application/json");
  request.send();
  this.valor = JSON.parse(request.responseText);
  resultado = this.formato(this.valor.result);
  return resultado;
}

formato (valor){
  var options = {
    eur: {
      style: "currency",
      currency: "EUR",
      minimumFranctionDigits: 0
    },
    ars: {
      style: "currency",
      currency: "ARS",
      minimumFranctionDigits: 0
    },
    mxn: {
      style: "currency",
      currency: "MXN",
      minimumFranctionDigits: 0
    }
  }
    var formateado = new Intl.NumberFormat("es-MX", options['mxn']).format(valor,2);
    return formateado

}

  }

  window.customElements.define(MySaldos.is, MySaldos);
