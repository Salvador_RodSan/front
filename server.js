//autor Salvador
var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

var path = require('path');
app.use(express.static(__dirname+'/build/es6-unbundled'));

app.listen(port);

console.log('Derechos reservados Salvador Rodriguez Sanchez: ' + port);


app.get("/", function(req, res) {
  res.sendFile("index.html",{root:'.'});
});
